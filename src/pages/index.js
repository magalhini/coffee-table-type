/* global graphql */

import React from 'react';
import Renard from '../components/Renard';
import RevueForm from '../components/Revue';
import Logo from '../assets/logo.svg';

const IndexPage = props =>
  (<main className="container">
    <div className="row section section-intro">
      <img className="main-logo" src={Logo} alt="Coffee Table Typography, logo" />
      <span className="h-v-hidden">Coffee Table Typography</span>
      <div className="col-sm-12 col-md-8">
        <h3 className="section-intro__title">We love words. And coffee. </h3>
        <p className="section-intro__desc">
        A <a href="https://www.getrevue.co/profile/coffeetabletypography/issues/53-bearded-frogs-in-metal-type-113124">newsletter</a> with curated resources about the science and the beauty of typography; in design, on the web, languages, and type history. You will read it over your morning cup of coffee, while the aromas of freshly ground beans are still in the air, quickly realising that words are beautiful and you might need a second cup after all.</p>

        <p><i>Need to take a look first? Here's <a href="https://www.getrevue.co/profile/coffeetabletypography/issues/53-bearded-frogs-in-metal-type-113124">issue #53, available online.</a></i></p>
      </div>
      <div className="col-sm-8 col-md-4">
        <Renard />
      </div>
    </div>

    <div className="row section">
      <div className="col-md-9">
        <h3>Subscribe to the newsletter</h3>

        <p className="">CTT is more than just an <i>email with links</i>. You'll receive a long form email with a crafted summary of just a few
        articles about type, or languages, or fonts, alongside news and of course, a coffee article.
        Issues are delivered every 2 or 3 weeks — and nothing else, ever.</p>
        <RevueForm />
      </div>
    </div>

    <hr />

    <div className="row section section-patreon">
      <div className="col-md-9">
        <h3>Patreon supporters</h3>
        <p><i>Coffee Table Typography</i> is maintained by <a href="https://twitter.com/magalhini">Ricardo Magalhães</a>, but I wouldn’t be able to continue without the support
        and love of the community. A special shoutout to all the wonderful people kind enough to believe in
        the work enough and support it on <a href="http://patreon.com/coffeetabletype">Patreon</a> <span role="img" aria-label="Heart emoji">❤️</span></p>

        <p><a href="http://patreon.com/coffeetabletype">Patreon supporters</a> will receive exclusive stickers, news,
        and early access to typography articles that I write. They'll also get coffee on me (or tea)
        every time I meet them in real life, of course.</p>

        <p>Huge thanks to: <span role="img" aria-label="Praying hands emoji">🙏</span></p>
        <ul className="patreon-list col-sm-10 col-md-12">
          {props.data.allDataJson.edges[0].node.patreons.map(patron =>
            (<li key={patron.name} className="patreon-name">
                <a href={patron.url}>{patron.name}</a>
              </li>
            ))}
        </ul>
      </div>

    </div>
  </main>
);

export default IndexPage;

export const pageQuery = graphql`
  query IndexQuery {
    allDataJson {
      edges {
        node {
          patreons {
            name
            url
          }
        }
      }
    }
  }
`;
