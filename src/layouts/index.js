/* global document */

import React from 'react';
import PropTypes from 'prop-types';
import Helmet from 'react-helmet';
import Footer from '../components/footer';

import '../../sass/style.scss';

const init = () => {
  if (typeof document !== 'undefined') {
    document.body.classList.add('loaded');
  }
}

// Never do this. Seriously.
// But Gatsby doesn't let you edit the HTML.
const addTypekit = () => {
  if (typeof document !== 'undefined') {
    const link = document.createElement('link');
    link.href = 'https://use.typekit.net/dyb6ehm.css';
    link.type = 'text/css';
    link.rel = 'stylesheet';
    link.media = 'screen, print';

    document.getElementsByTagName('head')[0].appendChild(link);
  }
};

init();

const TemplateWrapper = ({ children }) => (
  <div>
    <Helmet>
      <title>Coffee Table Typography</title>
      <meta name="description" content="Coffee table typography" />
      <base href="/" />
      <link rel="stylesheet" href="https://use.typekit.net/dyb6ehm.css" />
    </Helmet>
    {children()}
    <Footer />
  </div>
);

TemplateWrapper.propTypes = {
  children: PropTypes.func,
};

export default TemplateWrapper;
