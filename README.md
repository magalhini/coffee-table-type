# Coffee Table Typography Landing Page

⚠️ This page was built in just under 6 hours in total and is NOT a reflection of good development practices.
It's scrappy, and it could very well have been a static page — but of course I wanted to play with Gatsby a bit more and over-engineer the hell out of a landing page.

Here be developer's dragons, ye be warned.

### Running the project

It's late and I will update this later, for sure. I am not fixing this problem with coffee at 10pm, no sir.

1. Install Gatsby
2. Clone the repo
3. Run `yarn install`
4. Run `gastby develop` to start development mode
